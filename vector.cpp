

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
#include <math.h>
#include <conio.h>


#include "vector.h"


using namespace std;

void vector::show(int mark) {
    printf("(%d):\t", mark);
    printf("x = %.2f\ty = %.2f\t", x, y);
    printf("r = %.2f\t%.2f deg\t%.2f rad\n", r, adeg, arad);
}

void vector::show(string msg) {
    cout<<"("<<msg<<"):";
    printf("x = %.2f\ty = %.2f\t", x, y);
    printf("r = %.2f\t%.2f deg\t%.2f rad\n", r, adeg, arad);
}

vector::vector(float x, float y) {
    setXY(x, y);
}

vector::vector(void) {
    setXY(0.0, 0.0);
}

vector::vector(float r, float angle, angle_type_t type) {
    if(r < 0) {
        setXY(0.0, 0.0);
        return;
    }
    setRA(r, angle, type);
}

void vector::setX(float x) {
    this->x = x;
    xy2ra(); // re-calculate r and angle
}

void vector::setY(float y) {
    this->y = y;
    xy2ra(); // re-calculate r and angle
}

void vector::setXY(float x, float y) {
    this->x = x;
    this->y = y;
    xy2ra(); // re-calculate r and angle
}

void vector::setR(float r) {
    if(r < 0) return;
    this->r = r;
    ra2xy(); // re-calculate x and y
}

void vector::setDeg(float angle_deg) {
    adeg= angle_deg;
    arad = adeg / 180 * PI;
    ra2xy(); // re-calculate x and y
}

void vector::setRad(float angle_rad) {
    arad = angle_rad;
    adeg = arad / PI * 180;
    ra2xy(); // re-calculate x and y
}

void vector::setRA(float r, float angle, angle_type_t type) {
    if(r < 0) return;

    this->r = r;
    if(type == angle_type_t::rad) {
        setRad(angle);
    }else{
        setDeg(angle);
    }
}

void vector::swapXY(void) {
    float tmp = x;
    x = y, y = tmp;
    xy2ra();
}

float vector::getX(void) {
    return x;
}

float vector::getY(void) {
    return y;
}

float vector::getR(void) {
    return r;
}

float vector::getArad(void) {
    return arad;
}

float vector::getAdeg(void) {
    return adeg;
}

vector vector::add(vector op2) {
    vector tmp(this->x + op2.x, this->y + op2.y);
    return tmp;
}

vector vector::sub(vector op2) {
    vector tmp(this->x - op2.x, this->y - op2.y);
    return tmp;
}

vector vector::mul(vector op2) {
    vector tmp(this->r * op2.r, this->adeg + op2.adeg,\
    angle_type_t::deg);
    return tmp;
}

vector vector::div(vector op2) {
    const float LIMITED = 0.0000001;
    if(op2.r > -LIMITED && op2.r < LIMITED) {
        return *this; // r too small, almost 0
    }
    vector tmp(this->r / op2.r, this->adeg - op2.adeg,\
    angle_type_t::deg);
    return tmp;
}

vector vector::operator +(vector op2) {
    return this->add(op2);
}

vector vector::operator -(vector op2) {
    return this->sub(op2);
}

vector vector::operator *(vector op2) {
    return this->mul(op2);
}

vector vector::operator /(vector op2) {
    return this->div(op2);
}

vector vector::operator *(float op2) {
    vector tmp(op2, 0, angle_type_t::deg);
    return ((*this) * tmp);
}

vector vector::operator /(float op2) {
    vector tmp(op2, 0, angle_type_t::deg);
    return ((*this) / tmp);
}

// ==== below are private ========= //

void vector::xy2ra(void) {
    /**
     * @brief quadrant
     * 1,2,3,4 for I,II,III, IV.
     * 14 for x+, 23 for x-.
     * 12 for y+, 34 for y-.
     * 0 for origin
     **/
    int quad;

    r =  pow(x*x + y*y, 0.5); // power(base, index)

    if(x>0 && y>0) {
        quad = 1;
    }else if(x<0 && y>0) {
        quad = 2;
    }else if(x<0 && y<0) {
        quad = 3;
    }else if(x>0 && y<0) {
        quad = 4;
    }else if(x>0) {
        quad = 14;
    }else if(x<0) {
        quad = 23;
    }else if(y>0) {
        quad = 12;
    }else if(y<0) {
        quad = 34;
    }else {
        quad = 0;
    }

    do {
        if(quad == 0) {
           adeg = 0;
           break;
        }
        if(quad == 14) {
            adeg = 0;
            break;
        }
        if(quad == 23) {
            adeg = 180;
            break;
        }
        if(quad == 12) {
            adeg = 90;
            break;
        }
        if(quad == 34) {
            adeg = 270;
            break;
        }

        float radtmp = atanf(fabsf(y)/fabsf(x));    // arc_tan, abs
        float degtmp = radtmp / PI * 180;
        switch(quad) {
            case 1: {
                adeg = degtmp;
                break;
            }
            case 2: {
                adeg = 180 - degtmp;
                break;
            }
            case 3: {
                adeg = 180 + degtmp;
                break;
            }
            case 4: {
                adeg = 360 - degtmp;
                break;
            }
        }
    }while(0);
    // so, adeg is in range of [0, 360) deg

    arad = adeg / 180 * PI; // range of [0, 2*PI)
}

void vector::ra2xy(void) {
    unifyAngle();

    x = r * cosf(arad);
    y = r * sinf(arad);
}

void vector::unifyAngle(void) {
    int n;
    float deg = adeg;

    // map to [0, 360) deg
    if(deg < 0) {
        n = int(fabsf(deg) / 360) + 1;
        deg += (n * 360);
    }
    if(deg >= 360) {
        n = int(deg / 360);
        deg -= (n * 360);
    }

    adeg = deg;
    arad = deg * PI / 180;;
}
