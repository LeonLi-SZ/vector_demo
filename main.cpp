

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
#include <math.h>
#include <conio.h>

#include "vector.h"

using namespace std;

static inline void lr(void) {cout<<endl;}
static inline void lr(string msg) {cout<<"==== "<<msg<<" ===="<<endl;}
#define lrs() cout<<"\n=============\n\n"

int main (void) {
    cout<<__DATE__<<"; "<<__TIME__<<"\n\n";
    vector v1, v2, vrslt;

    v1.setXY(6, 8), v2.setXY(2, 7);
    vrslt = v1 + v2;
    v1.show("v1"), v2.show("v2"), vrslt.show("v1 + v2");

    lrs();


    vrslt = v1 - v2;
    v1.show("v1"), v2.show("v2"), vrslt.show("v1 - v2");

    lrs();

    v1.setRA(8, 40, angle_type_t::deg), v2.setRA(2, 70, angle_type_t::deg);
    vrslt = v1 * v2;
    v1.show("v1"), v2.show("v2"), vrslt.show("v1 * v2");

    lrs();

    vrslt = v1 / v2;
    v1.show("v1"), v2.show("v2"), vrslt.show("v1 / v2");

    lrs();
    vrslt = v1 * 4;
    v1.show("v1"), vrslt.show("v1 * 4");

    lrs();
    vrslt = v1 / 4;
    v1.show("v1"), vrslt.show("v1 / 4");

    return 0;
}
