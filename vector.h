#pragma once

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
#include <math.h>
#include <conio.h>

using namespace std;

/*
DevCpp will show warning:
[Warning] scoped enums only available with -std=c++11 or -std=gnu++11

so, need to:
Project - Project Options... - Parameters
in the C++ compiler tab, add below line

-std=c++11
*/

/**
 * @brief angle type: rad or deg
 * */
enum class angle_type_t{
    rad,
    deg,
};

static const float PI = 3.1415926;

class vector {
    public:
        vector(float x, float y);
        vector(void);
        vector(float r, float angle, angle_type_t type);

        void setX(float x);
        void setY(float y);
        void setXY(float x, float y);
        void setR(float r);
        void setDeg(float angle_deg);
        void setRad(float angle_rad);
        void setRA(float r, float angle, angle_type_t type);
        void swapXY(void);

        float getX(void);
        float getY(void);
        float getR(void);
        float getArad(void);
        float getAdeg(void);

        vector add(vector op2);
        vector sub(vector op2);
        vector mul(vector op2);
        vector div(vector op2);

        vector operator +(vector op2);
        vector operator -(vector op2);
        vector operator *(vector op2);
        vector operator /(vector op2);

        vector operator *(float op2);
        vector operator /(float op2);

        /**
         * @brief show infor of the vector
         * @param mark a number for reference index
         * @return none
         * */
        void show(int mark);

        /**
         * @brief show infor of the vector
         * @param msg a message for reference
         * @return none
         * */
        void show(string msg);

    private:
        float x;
        float y;
        float r;
        float arad, adeg;

        /**
         * @brief x,y --> r and angle
         * @param none
         * @return none
         * */
        void xy2ra(void);

        /**
         * @brief r,angle --> x and y
         * @param none
         * @return none
         * */
        void ra2xy(void);

        /**
         * @brief unify angle to [0, 360) deg, AKA [0, 2*PI) rad
         * @param none
         * @return none
         * */
        void unifyAngle(void);

};
